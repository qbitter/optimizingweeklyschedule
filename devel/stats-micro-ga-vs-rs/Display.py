import Constants 

import datetime
import json #used for pretty printing

def schedule_to_latex(schedule):
    """
    Converts a weekly schedule to a human readable LATEX table.
    
    :param schedule: The weekly schedulte to convert
    :type schedule: sequence
    :return: LATEX table generated from schedule.
    :rtype: str
    """

    table = ""
    table_start = """
    \\begin{tabular}{c|c|c|c|c|c|c|c}
    \\arrayrulecolor{white} 
    \\textbf{{Hour}} & """
    for day in Constants.Weekdays:
        table_start += f"\\textbf{{{day.name}}} & "  
    
    table_start = table_start[0:-3] + " \\\\\n"

    table_end = """
    \\end{tabular}
    """
    row_end = "\\\\\n\\hline\n"

    table += table_start
    
    for i in range(24):
        row = f"\t{datetime.time(i).strftime('%I:00 %p')} & "

        for j in range(7):
            activity = schedule[i+24*j]
            row += f"\\cellcolor{{{activity.color}}}{activity.name} & "
        
        row = row[:-2] 
        row += row_end
        table += row
    table += table_end

    return table


def schedule_to_html(schedule):
    """
    Converts a weekly schedule to a human readable HTML table.
    
    :param schedule: The weekly schedulte to convert
    :type schedule: sequence
    :return: HTML table generated from schedule.
    :rtype: str
    """

    table = ""
    table_start = """
    <table>
    <tr>
    <th> Hour </th>
    """
    for day in Constants.Weekdays:
        table_start += f"""
        <th> {day.name} </th>
        """   
    table_start += f"""
    </tr>
    """
 
    table_end = """
    </table>
    """
    row_start = """
    <tr>
    """
    row_end = """
    </tr>
    """

    table += table_start
    for i in range(24):
        table += row_start
        table += f"""
            <td> {datetime.time(i).strftime("%I:00 %p")} </td>
        """
        for j in range(7):
            activity = schedule[i+24*j]

            table += f"""
            <td style="background-color:{activity.color};"> {activity.name} </td>
            """
        table += row_end 
    table += table_end

    return table


def schedule_happiness_to_html(schedule, happiness, happiness_log):
    print("""
    <style>
    div {
        white-space: pre-wrap;
    }
    </style>
    """)
    print("#"*80)
    print("<br />")
    print(f"Happiness {happiness}")
    print(schedule_to_html(schedule))
    print("<div>")
    print(json.dumps(happiness_log, indent=4))
    print("</div>")
    print("#"*80)
    print("<br />")
