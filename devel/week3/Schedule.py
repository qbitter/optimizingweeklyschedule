import Constants
import Activity

class Schedule:
    def __init__(self):
       self.schedule = [None]*Constants.weekHours 

    def set(self, hour, activity):
        self.schedule[hour] = Activity.Activity(activity.name, activity.color, 1)

    def get(self, hour):
        if self.schedule[hour] is None:
            return Activity.Activity("Unknown", "Lavender", 1)
        else:
            return self.schedule[hour]

    def __getitem__(self, hour):
        return self.get(hour)

    def __setitem__(self, hour, activity):
        return self.set(hour, activity)

    def hours_available(self):
        """
        Returns the number of hours that have no activity assigned.
        """
        return sum(map(lambda x: x is None, self.schedule))

    def n_available(self, n):
        """
        Returns all slots which do not have assigned n consecutive hours.
        """

        slots = list()
        for i in range(Constants.weekHours - n + 1):
            if all(map(lambda x: x is None, self.schedule[i:i+n])):
                slots.append(i)

        return slots

    def activity_slots(self, activity):
        """
        Returns all slots at which a given activity is scheduled and for how long.
        """
        slots = dict()

        hour = 0
        while hour < Constants.weekHours: 
            if (self.schedule[hour] is not None) and self.schedule[hour].name == activity:
                start = hour
                duration = 0
                while (hour < Constants.weekHours) and (self.schedule[hour] is not None) and self.schedule[hour].name == activity:
                    duration += 1
                    hour += 1

                slots[start] = duration

            hour += 1

        return slots







