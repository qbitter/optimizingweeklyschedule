import yaml

def happiness(schedule):
    happiness = 0
    log = dict()

    with open("activities.yaml", 'r') as fd:
        activities = yaml.safe_load(fd)
    
    for activity, properties in activities['activities'].items():
        #homework {'general': 50, 'max_time': 3, 'break_time': 2, 'daytime_fit': {7: 0.1, 8: 0.3, 9: 1.0, 10: 1.0, 11: 1.0, 12: 1.0, 13: 1.0, 14: 1.0, 15: 1.0, 16: 1.0, 17: 0.75, 18: 0.5, 19: 0.3, 20: 0.1, 21: 0.1}}
        hp_properties = properties['happiness']

        #key is starting time in absolut hours, value is duration at this point
        slots = schedule.activity_slots(activity)
    
        #Activity is not scheduled. Does not influence overall happiness.
        if not slots:
            continue

        #Check for how long this activity is done. Punish overtime of the activity.  
        def max_time_factor():
            factor = 1.0 
            for duration in slots.values():
                overtime = duration - hp_properties['max_time']
                #Punish overtime exponentially.
                if overtime > 0:
                    factor /= 2**overtime

            return factor

        #Check if the break between one activity is big enough.
        def break_time_factor():
            factor = 1.0

            #The activity does not influence the schedule at all if it is only scheduled once. 
            if len(slots) == 1:
                return factor

            start_end = tuple((k, k+v) for k, v in slots.items()) 
            #Start at -1 to model the week arround wrap up
            for i in range(0, len(start_end)-1):
                break_duration = abs(start_end[i+1][0] - start_end[i][1])
                if break_duration < hp_properties['break_time']:
                    factor *= (break_duration / hp_properties['break_time'])

            return factor
           
        #Check how good this activity is scheduled in the day. E.g.: one may does not like to do sport straight after breakfast.
        def daytime_fit_factor():
            factor = 1.0

            day_fitness = list()
            hours_measured = 0
            for start, duration in slots.items():
                start_daytime = start % 24

                for i in range(duration):
                    day_fitness.append(hp_properties['daytime_fit'][start_daytime+i])
                    hours_measured += 1
            
            factor *= (sum(day_fitness) / hours_measured)

            return factor

        #Calculate Happiness with the scheduling of this activity.
        max_time = max_time_factor()
        break_time = break_time_factor()
        daytime_fit = daytime_fit_factor()

        happiness_factor = (1/3) * break_time + (1/3) * max_time + (1/3) * daytime_fit

        activitiy_happiness = sum(slots.values())*properties['happiness']['general']*happiness_factor 

        #Log Activities Happiness Calculation 
        log[activity] = {
            "max_time": max_time,
            "break_time": break_time,
            "daytime_fit": daytime_fit,
            "happiness_factor": happiness_factor,
            "activitiy_happiness": activitiy_happiness
        }
 
        happiness += activitiy_happiness
        
       
    return log, happiness


