import Generate
import Happiness
import Display

import json
import multiprocessing

def gen_and_measure(*args, **kwargs):
    s = Generate.gen()
    happiness_log, happiness = Happiness.happiness(s)
    return s, happiness, happiness_log

schedules = [None]*10**3
with multiprocessing.Pool(processes=8) as pool:
    schedules = pool.map(gen_and_measure, schedules)   

#sorted_schedules = sorted(schedules, key=lambda x: x[1], reverse=True)

measure_happiness = dict()

"""
log[activity] = {
            "max_time": max_time,
            "break_time": break_time,
            "daytime_fit": daytime_fit,
            "happiness_factor": happiness_factor,
            "activitiy_happiness": activitiy_happiness
        }
"""

for schedule in schedules:
    for activity, log in schedule[2].items():
        if activity in measure_happiness:
            measure_happiness[activity]["max_time"].append(log["max_time"])
            measure_happiness[activity]["break_time"].append(log["break_time"])
            measure_happiness[activity]["daytime_fit"].append(log["daytime_fit"])
        else:
            measure_happiness[activity] = dict()
            measure_happiness[activity]["max_time"] = [log["max_time"]]
            measure_happiness[activity]["break_time"] = [log["break_time"]]
            measure_happiness[activity]["daytime_fit"] = [log["daytime_fit"]]


per_activity_stretch_average = dict()
per_activity_stretch_average["max_time"] = 0
per_activity_stretch_average["break_time"] = 0
per_activity_stretch_average["daytime_fit"] = 0

for activity in measure_happiness:
    #print(activity)
    happiness_factors_average = 0
    sums = dict()    

    for factor, value in measure_happiness[activity].items():
        s = sum(value)
        happiness_factors_average += s
        sums[factor] = s
        #print(f"{factor} has influence {s}")

    #print(f"Average {happiness_factors_average/3}")
    for factor, val in sums.items():
        #print(f"{factor} counts {val/happiness_factors_average} percent")
        #print(f"{factor} stretch should be {((1/3) / (val/happiness_factors_average))*1/3}")
        per_activity_stretch_average[factor] += ((1/3) / (val/happiness_factors_average)) * (1/3)

for factor, s in per_activity_stretch_average.items():
    print(f"{factor} activities average is {s/len(measure_happiness):.3f}")


