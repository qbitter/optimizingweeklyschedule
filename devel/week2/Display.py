import Constants 

import datetime

def schedule_to_html(schedule):
    """
    Converts a weekly schedule to a human readable HTML table.
    
    :param schedule: The weekly schedulte to convert
    :type schedule: sequence
    :return: HTML table generated from schedule.
    :rtype: str
    """

    table = ""
    table_start = """
    <table>
    <tr>
    <th> Hour </th>
    """
    for day in Constants.Weekdays:
        table_start += f"""
        <th> {day.name} </th>
        """   
    table_start += f"""
    </tr>
    """
 
    table_end = """
    </table>
    """
    row_start = """
    <tr>
    """
    row_end = """
    </tr>
    """

    table += table_start
    for i in range(24):
        table += row_start
        table += f"""
            <td> {datetime.time(i).strftime("%I:00 %p")} </td>
        """
        for j in range(7):
            activity, color = schedule[i+24*j]

            table += f"""
            <td style="background-color:{color};"> {activity} </td>
            """
        table += row_end 
    table += table_end

    return table


