import Constants

import dataclasses

@dataclasses.dataclass
class Entry:
    name: str
    color: str

class Schedule:
    def __init__(self):
       self.schedule = [None]*Constants.weekHours 

    def set(self, hour, activity, color):
        self.schedule[hour] = Entry(activity, color)

    def get(self, hour):
        if self.schedule[hour] is None:
            return "Unknown", "Lavender"
        else:
            return self.schedule[hour].name, self.schedule[hour].color  

    def __getitem__(self, hour):
        return self.get(hour)

    def __setitem__(self, hour, entry):
        return self.set(hour, entry.name, entry.color)

    def hours_available(self):
        """
        Returns the number of hours that have no activity assigned.
        """
        return sum(map(lambda x: x is None, self.schedule))

    def n_available(self, n):
        """
        Returns all slots which do not have assigned n consecutive hours.
        """

        slots = list()
        for i in range(Constants.weekHours - n + 1):
            if all(map(lambda x: x is None, self.schedule[i:i+n])):
                slots.append(i)

        return slots

    def activity_slots(self, activity):
        """
        Returns all slots at which a given activity is schedule and for how long.
        """
        slots = dict()

        hour = 0
        while hour < Constants.weekHours: 
            if (self.schedule[hour] is not None) and self.schedule[hour].name == activity:
                start = hour
                duration = 0
                while (hour < Constants.weekHours) and (self.schedule[hour] is not None) and self.schedule[hour].name == activity:
                    duration += 1
                    hour += 1

                slots[start] = duration

            hour += 1


        return slots







