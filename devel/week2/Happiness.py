import Generate

import yaml

def happiness(schedule):
    happiness = 0

    with open("activities.yaml", 'r') as fd:
        activities = yaml.safe_load(fd)
    
    for activity, properties in activities['activities'].items():
        #homework {'general': 50, 'max_time': 3, 'break_time': 2, 'daytime_fit': {7: 0.1, 8: 0.3, 9: 1.0, 10: 1.0, 11: 1.0, 12: 1.0, 13: 1.0, 14: 1.0, 15: 1.0, 16: 1.0, 17: 0.75, 18: 0.5, 19: 0.3, 20: 0.1, 21: 0.1}}
        hp_properties = properties['happiness']

        #key is starting time in absolut hours, value is duration at this point
        slots = schedule.activity_slots(activity)
    
        #Activity is not scheduled. Does not influence overall happiness.
        if not slots:
            continue
  
        def max_time_factor():
            factor = 1.0 
            for duration in slots.values():
                overtime = duration - hp_properties['max_time']
                #Punish overtime exponentially.
                if overtime > 0:
                    factor *= 2**overtime

            return factor
 
        def break_time_factor():
            factor = 1.0

            #No influence as only schedule once.
            if len(slots) == 1:
                return factor

            start_end = tuple((k, k+v) for k, v in slots.items()) 
            for i in range(len(start_end)-1):
                distance = start_end[i+1][0] - start_end[i][1]
                if distance < hp_properties['break_time']:
                   factor *= distance / hp_properties['break_time']

            return factor
            
        def daytime_fit_factor():
            factor = 1.0 
            for start, duration in slots.items():
                start_daytime = start % 24
                for i in range(duration):
                    factor *= hp_properties['daytime_fit'][start_daytime+i]

            return factor

        max_time = max_time_factor()
        break_time = break_time_factor()
        daytime_fit = daytime_fit_factor()

        happiness_factor = break_time * max_time * daytime_fit
 
        happiness += sum(slots.values())*properties['happiness']['general']*happiness_factor
        
       
    return happiness


