import Generate
import Happiness
import Display


import multiprocessing

def f(*args, **kwargs):
    s = Generate.gen()
    return s, Happiness.happiness(s)

schedules = [None]*10**5
with multiprocessing.Pool(processes=8) as pool:
    schedules = pool.map(f, schedules)   

#schedules = [Generate.gen() for _ in range(10**4)]

#sorted_schedules = sorted(list(map(lambda x: (x, Happiness.happiness(x)), schedules)), key=lambda x: x[1], reverse=True)
sorted_schedules = sorted(schedules, key=lambda x: x[1], reverse=True)

for schedule in sorted_schedules[0:2]:
    print("#"*80)
    print("<br />")
    print(f"Happiness {schedule[1]}")
    print(Display.schedule_to_html(schedule[0]))
    print("#"*80)
    print("<br />")
