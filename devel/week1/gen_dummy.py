import constants
import display

import yaml
import random

random.seed()

with open("activities.yaml", 'r') as fd:
    d = yaml.safe_load(fd)

must = list()
optional = list()

for activity, properties in d['activities'].items():
    for _ in range(properties['times']['min'] * properties['block_duration']):
        must.append( (activity, properties['color']) )

    for _ in range((properties['times']['max'] - properties['times']['min']) * properties['block_duration']):
        optional.append( (activity, properties['color']) )

schedule = must.copy()

for _ in range(constants.weekHours - len(schedule)):
    schedule.append( optional.pop( random.randrange(len(optional)) ) )

random.shuffle(schedule)

print(display.schedule_to_html(schedule))
