import constants 

def schedule_to_html(schedule):
    """
    Converts a weekly schedule to a human readable HTML table.
    
    :param schedule: The weekly schedulte to convert
    :type schedule: sequence
    :return: HTML table generated from schedule.
    :rtype: str
    """

    assert(len(schedule) == 168)  

    table = ""
    table_start = """
    <table>
    <tr>
    <th> Hour </th>
    """
    for day in constants.Weekdays:
        table_start += f"""
        <th> {day.name} </th>
        """   
    table_start += f"""
    </tr>
    """
 
    table_end = """
    </table>
    """
    row_start = """
    <tr>
    """
    row_end = """
    </tr>
    """

    table += table_start
    for i in range(24):
        table += row_start
        table += f"""
            <td> {i} </td>
        """
        for j in range(7):
            activity = schedule[i+24*j][0] 
            color = schedule[i+24*j][1]

            table += f"""
            <td style="background-color:{color};"> {activity} </td>
            """
        table += row_end 
    table += table_end

    return table


