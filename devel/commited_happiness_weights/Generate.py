import Constants
import Display
import Schedule
import Activity

import yaml
import random


def gen():
    schedule = Schedule.Schedule()

    with open("activities.yaml", 'r') as fd:
        activities = yaml.safe_load(fd)

    #Fixed Activities (sleep, eat, etc.)
    for activity, properties in activities['mandatory_activities'].items():
        for start_time in properties['start_times']:
            for i in range(properties['duration']):
                schedule[(start_time+i)%Constants.weekHours] = Activity.Activity(activity, properties['color'], 1)

    #Activities
    must = list()
    optional = list()

    #Read activities
    for activity, properties in activities['activities'].items():
        #must
        for i in range(properties['times']['min']):
            must.append(Activity.Activity(activity, properties['color'], properties['block_duration']))
            
        #optional
        for i in range( properties['times']['max'] - properties['times']['min']):
            optional.append(Activity.Activity(activity, properties['color'], properties['block_duration']))


    #Distribute
    assert sum([x.duration for x in must]) <= schedule.hours_available(), "Too many must activities. Cannot schedule them all."

    #must
    while must:
        activity = must.pop() 
        slots = schedule.n_available(activity.duration)

        if not slots:
            raise RuntimeError("Unable to schedule all must slots.")

        slot = random.choice(slots)
        for i in range(activity.duration):
            schedule[slot+i] = Activity.Activity(activity.name, activity.color, 1)

    #optional
    random.shuffle(optional)

    while (schedule.hours_available() > 0) and optional:
        activity = optional.pop() 
        slots = schedule.n_available(activity.duration)
        
        if not slots:
            continue

        slot = random.choice(slots)
        for i in range(activity.duration):
            schedule[slot+i] = Activity.Activity(activity.name, activity.color, 1) 

    return schedule


if __name__ == '__main__':
    schedule = gen()
    print(Display.schedule_to_html(schedule))





