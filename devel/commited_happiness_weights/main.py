import Generate
import Happiness
import Display

import json #pretty printing
import multiprocessing

def gen_and_measure(*args, **kwargs):
    s = Generate.gen()
    happiness_log, happiness = Happiness.happiness(s)
    return s, happiness, happiness_log

schedules = [None]*10**3
with multiprocessing.Pool(processes=8) as pool:
    schedules = pool.map(gen_and_measure, schedules)   

sorted_schedules = sorted(schedules, key=lambda x: x[1], reverse=True)


print("""
<style>
div {
    white-space: pre-wrap;
}
</style>
""")
for schedule in sorted_schedules[0:2]:
    print("#"*80)
    print("<br />")
    print(f"Happiness {schedule[1]}")
    print(Display.schedule_to_html(schedule[0]))
    print("<div>")
    print(json.dumps(schedule[2], indent=4))
    print("</div>")
    print("#"*80)
    print("<br />")
