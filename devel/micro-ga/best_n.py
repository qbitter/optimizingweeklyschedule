import Generate
import Happiness
import Display
import Constants
import Schedule

import multiprocessing
import random
import dataclasses
import json

#Wait for so many rounds before the solution is considered converged
rounds_considered_converged = 25

best_candidates = dict()

@dataclasses.dataclass
class Solution:
    schedule : Schedule
    happiness : int
    happiness_log : dict

def mga(N):
    population = list()

    #create initial population
    for _ in range(N):
        s = Generate.gen()
        happiness_log, happiness = Happiness.happiness(s)
        population.append(Solution(s, happiness, happiness_log))

  
    #TODO abort if local maximum is reached 
    #run "evolution" process
        
    population = sorted(population, key=lambda x: x.happiness, reverse=True)
    best_happiness = population[0].happiness 

    assert best_happiness > 0

    def abort_cond(max_happiness):
        if abort_cond.old_happiness < max_happiness:
            abort_cond.old_happiness = max_happiness
            abort_cond.i = 0
        else:
            abort_cond.i += 1

        return True if abort_cond.i < rounds_considered_converged else False  
             
    abort_cond.old_happiness = 0

    #This loop expects the population to be sorted descending beased on their happiness score         
    steps = 0  
    while abort_cond(best_happiness):
        steps += 1
        
        #print(f"Step {steps} - Best Solution {population[0].happiness}")
 
        #evict worst solution 
        population = population[0:-1] 

        #crossover
        #keep best solution
        new_population = [population[0]]
        
        #MAGIC Constants 7 and -2.
        #It shall be assured that the split makes a difference.
        #Therefore the schedule shall not be splited in the fixed part on monday morning (sleep, eat) and the fixed part on sunday evening (sleep) 
        cut_position = random.randrange(7, Constants.weekHours-2)

        fst_parts = list()
        snd_parts = list()

        #TODO How to assure two splits from the same parent are not again reconnected?
        for solution in population:
            fst_parts.append(solution.schedule.schedule[0:cut_position])
            snd_parts.append(solution.schedule.schedule[cut_position:])

        for _ in range(N-1):
            a = random.choice(fst_parts)
            b = random.choice(snd_parts)
            new_schedule = Schedule.Schedule(a+b)
            new_happiness_log, new_happiness = Happiness.happiness(new_schedule)

            new_population.append(Solution(new_schedule, new_happiness, new_happiness_log)) 

        #kill the old generation
        population = new_population
    
        #sort population for next round, recalculate best happiness
        population = sorted(population, key=lambda x: x.happiness, reverse=True)
        best_happiness = population[0].happiness 

    best_candidates[N].append( (i, population[0]) )
      
 
for i in range(2, 25):
    best_candidates[i] = list()
    for _ in range(3):
        mga(i)

    #best_candidates[i] = list(sorted(map(lambda x: x[1].happiness, best_candidates[i]), reverse=True))
    best_candidates[i] = list(map(lambda x: x[1].happiness, best_candidates[i]))


#print res
for i in range(3):
    print("( ", end="")
    for k, v in best_candidates.items():
        print(f"({k}, {v[i]}), ", end="")
    print(")")


