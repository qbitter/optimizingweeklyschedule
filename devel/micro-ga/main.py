import Generate
import Happiness
import Display

import multiprocessing

def gen_and_measure(*args, **kwargs):
    s = Generate.gen()
    happiness_log, happiness = Happiness.happiness(s)
    return s, happiness, happiness_log

schedules = [None]*10**3
with multiprocessing.Pool(processes=8) as pool:
    schedules = pool.map(gen_and_measure, schedules)   

sorted_schedules = sorted(schedules, key=lambda x: x[1], reverse=True)


#for schedule in sorted_schedules[0:2]:
#    Display.schedule_happiness_to_html(*schedule)
   

print(Display.schedule_to_latex(sorted_schedules[0][0]))
