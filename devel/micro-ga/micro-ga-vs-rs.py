import Generate
import Happiness
import Display
import Constants
import Schedule

import multiprocessing
import random
import dataclasses
import time

#best_candidates = []

@dataclasses.dataclass
class Solution:
    schedule : Schedule
    happiness : int
    happiness_log : dict

def mga(N, best_candidates):
    population = list()

    #create initial population
    for _ in range(N):
        s = Generate.gen()
        happiness_log, happiness = Happiness.happiness(s)
        population.append(Solution(s, happiness, happiness_log))

  
    #TODO abort if local maximum is reached 
    #run "evolution" process
        
    population = sorted(population, key=lambda x: x.happiness, reverse=True)
    best_happiness = population[0].happiness 

    assert best_happiness > 0

    def abort_cond(max_happiness):
        abort_cond.i = abort_cond.i - 1
        if abort_cond.old_happiness < max_happiness:
            abort_cond.i = abort_cond.i + round(((1 + (max_happiness / abort_cond.old_happiness))*N) - N)
            abort_cond.old_happiness = max_happiness

        return True if abort_cond.i > 0 else False  
             
    abort_cond.old_happiness = best_happiness
    abort_cond.i = 11

    #This loop expects the population to be sorted descending beased on their happiness score         
    steps = 0  
    while (steps < 1) or abort_cond(best_happiness):
        steps += 1
        
        #print(f"Step {steps} - Best Solution {population[0].happiness}")
 
        #evict worst solution 
        population = population[0:-1] 

        #crossover
        #keep best solution
        new_population = [population[0]]
        
        #MAGIC Constants 7 and -2.
        #It shall be assured that the split makes a difference.
        #Therefore the schedule shall not be splited in the fixed part on monday morning (sleep, eat) and the fixed part on sunday evening (sleep) 
        cut_position = random.randrange(7, Constants.weekHours-2)

        fst_parts = list()
        snd_parts = list()

        #TODO How to assure two splits from the same parent are not again reconnected?
        for solution in population:
            fst_parts.append(solution.schedule.schedule[0:cut_position])
            snd_parts.append(solution.schedule.schedule[cut_position:])

        for _ in range(N-1):
            a = random.choice(fst_parts)
            b = random.choice(snd_parts)
            new_schedule = Schedule.Schedule(a+b)
            new_happiness_log, new_happiness = Happiness.happiness(new_schedule)

            new_population.append(Solution(new_schedule, new_happiness, new_happiness_log)) 

        #kill the old generation
        population = new_population
    
        #sort population for next round, recalculate best happiness
        population = sorted(population, key=lambda x: x.happiness, reverse=True)
        best_happiness = population[0].happiness 

    best_candidates.append( population[0] )
      




def abort_cond():
    return True if time.time() < abort_cond.abort_time else False
abort_cond.abort_time = time.time()+7200

best_candidates = []
while abort_cond():
    mga(5, best_candidates)


l = list(sorted(best_candidates, key=lambda x: x.happiness, reverse=True))

with open("schedule.txt", "w") as fd:
    fd.write(str(l[0].schedule.schedule))

with open("scores.txt", "w") as fd:
    all_happiness_scores = list(map(lambda x: x.happiness, l))
    fd.write(str(all_happiness_scores))
