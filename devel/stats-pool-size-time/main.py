import matplotlib.pyplot as plt
import seaborn as sns
import math

measurements = [
    {2: 5239.734000000001, 3: 5305.009000000001, 4: 5062.652, 5: 5060.704000000001, 6: 5234.054000000001, 7: 5379.154, 8: 5325.929000000001, 9: 5195.542},
    {2: 5042.815000000001, 3: 5178.1445, 4: 5147.144000000001, 5: 5262.139, 6: 5328.97, 7: 5269.7255, 8: 5284.803000000001, 9: 5266.107000000001},
    {2: 5150.121000000001, 3: 5392.227000000001, 4: 5222.229, 5: 5230.395, 6: 5352.394, 7: 5282.915, 8: 5233.665, 9: 5171.888},
    {2: 5372.812500000001, 3: 5119.1359999999995, 4: 5080.159000000001, 5: 5072.928, 6: 5149.2875, 7: 5307.033, 8: 5169.199500000001, 9: 5295.0105},
    {2: 5135.008000000001, 3: 5163.638, 4: 5315.978, 5: 5147.1410000000005, 6: 5271.009, 7: 5089.884, 8: 5119.3730000000005, 9: 5259.994000000001},
    {2: 5248.292, 3: 5098.295, 4: 5115.1720000000005, 5: 5230.009000000001, 6: 5123.649, 7: 5165.664000000001, 8: 5115.941000000001, 9: 5341.5005},
    {2: 5215.0325, 3: 5331.85, 4: 5192.584000000001, 5: 5230.5785000000005, 6: 5284.469999999999, 7: 5114.859000000001, 8: 5339.166, 9: 5074.004},
    {2: 5050.115000000001, 3: 5124.118, 4: 5315.434000000001, 5: 5235.144, 6: 5455.123, 7: 5402.417, 8: 5281.586500000001, 9: 5284.7405},
    {2: 5047.872, 3: 5355.117, 4: 5294.739, 5: 5283.925000000001, 6: 5142.089000000001, 7: 5430.272000000001, 8: 5231.874000000001, 9: 5295.518999999999},
    {2: 5099.609, 3: 4990.566500000001, 4: 5162.009000000001, 5: 5161.3035, 6: 5201.664000000001, 7: 5109.905000000001, 8: 5332.084, 9: 5409.159},
    ]


#axes = list()
#data = list()
#
#for k in measurements[0]:
#    axes.append(k)
#    l = list()
#    for measurement in measurements:
#        l.append(measurement[k])
#
#    data.append(l)
#
#
#fig, ax = plt.subplots()
#ax.boxplot(data)
#ax.set_xticklabels(axes)
#
#plt.xlabel('Pool Size')
#plt.ylabel('Happiness Score')
#plt.show()


E = dict()
E2 = dict()
V = dict()
Min = dict()
Max = dict()

for k in measurements[0]:
    E[k] = 0
    E2[k] = 0
    Min[k] = 10**6 #Magic Constant 10**6: Happiness Score that is for sure higher than any observable happiness score
    Max[k] = 0
    for m in measurements:
        E[k] += m[k]
        E2[k] += m[k]**2
        Min[k] = min(Min[k], m[k])
        Max[k] = max(Max[k], m[k])
        
    E[k] *= 1/(len(measurements))
    E2[k] *= 1/(len(measurements))
    V[k] = math.sqrt(E2[k] - E[k]**2)
    
    
print(f"""
E: {E}
V: {V}
Mi: {Min}
Ma: {Max}
""")
    

#for measurement in measurements:
#    plt.scatter(measurement.keys(), measurement.values()) 
#
#plt.xlabel('Pool Size')
#plt.ylabel('Happiness Score')
#plt.show()

#i = 0
#for m in measurements:
#    holds = True
#    for j in range(6, 10):
#        holds = holds and (m[5] < m[j])
#
#    if holds:
#        i = i + 1
#    
#
#print(f"{i}")
