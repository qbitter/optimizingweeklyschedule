from Activity import Activity
from Schedule import Schedule
import Display
import Happiness

import yaml


current_schedule = [
    #Monday
    ["sleep"]*6,
    ["eat"],
    [],
    ["housework"],
    ["transit"],
    ["ethz"]*2,
    ["eat"],
    ["work"]*5,
    ["transit"],
    ["eat"],
    ["read"],
    ["read"],
    ["sleep"]*2,

    #Tuesday
    ["sleep"]*6,
    ["eat"],
    ["transit"],
    ["work"]*4,
    ["eat"],
    ["work"]*3,
    ["ethz"]*2,
    ["transit"],
    ["eat"],
    ["boulder"],
    ["boulder"],
    ["sleep"]*2,

    #Wednesday
    ["sleep"]*6,
    ["eat"],
    ["code"]*5,
    ["eat"],
    ["code"]*5,
    ["eat"],
    ["people"]*3,
    ["sleep"]*2,
    
    #Thursday
    ["sleep"]*6,
    ["eat"],
    ["code"]*3,
    ["ethz"]*2,
    ["eat"],
    ["homework"]*2,
    [],
    ["read"]*2,
    ["eat"],
    [],
    ["boulder"]*2,
    ["sleep"]*2,
   
    #Friday
    ["sleep"]*6,
    ["eat"],
    ["transit"],
    ["work"]*4,
    ["eat"],
    ["work"]*5,
    ["eat"],
    ["people"]*3,
    ["sleep"]*2,
 
    #Saturday
    ["sleep"]*6,
    ["eat"],
    ["read"]*3,
    ["code"]*2,
    ["eat"],
    ["boulder"]*2,
    ["people"]*3,
    ["eat"],
    ["people"]*3,
    ["sleep"]*2,

    #Sunday
    ["sleep"]*6,
    ["eat"],
    [],
    [],
    [],
    ["read"],
    ["read"],
    ["eat"],
    ["code"]*2,
    ["people"]*3,
    ["eat"],
    ["people"]*3,
    ["sleep"]*2,
]

def flatten(l):
    i = list()
    for j in current_schedule:
        if j != []:
            for k in j:
                i.append(k)
        else:
            i.append("unknown")

    return i

#print(len(flatten(schedule)))

schedule_flattened = flatten(current_schedule)

with open("activities.yaml", 'r') as fd:
    activities = yaml.safe_load(fd)

schedule = list()

for e in schedule_flattened:
    if e != "unknown":
        if e in activities["activities"]:
            schedule.append(Activity(e, color=activities["activities"][e]["color"], duration=1)) 
        elif e in activities["mandatory_activities"]:
            schedule.append(Activity(e, color=activities["mandatory_activities"][e]["color"], duration=1)) 
        else:
            raise RuntimeError(f"Unknown Activity {e}")
    else:
        schedule.append(None) 
     
s = Schedule(schedule)

print(Display.schedule_to_latex(s))
print(Happiness.happiness(s)[1])

