import dataclasses 

@dataclasses.dataclass
class Activity:
    name: str
    color: str
    duration: int 
