import Generate
import Happiness
import Display

import json #pretty printing
import multiprocessing
import time

#TODO TABOO LIST IMPLEMENTATION IS QUITE HACKY
taboo_list = list()

all_happiness_scores = list()

#def abort_cond():
#    abort_cond.counter = abort_cond.counter - 1
#    return True if abort_cond.counter >=0 else False
#abort_cond.counter = 10**3

def abort_cond():
    return True if time.time() < abort_cond.abort_time else False
abort_cond.abort_time = time.time()+7200

best_schedule = None
best_happiness = 0
best_happiness_log = None

while abort_cond():
    s = Generate.gen()

    if s not in taboo_list:
        taboo_list.append(s)
        happiness_log, happiness = Happiness.happiness(s)

        all_happiness_scores.append(happiness)

        if best_happiness < happiness:
            best_schedule = s
            best_happiness = happiness
            best_happiness_log = happiness_log

#schedule = f"""
#<style>
#div {{
#    white-space: pre-wrap;
#}}
#</style>
#{"#"*80}
#<br />
#Happiness {best_happiness}
#{Display.schedule_to_html(s)}
#<div>
#{json.dumps(best_happiness_log, indent=4)}
#</div>
#{"#"*80}
#<br />
#"""

with open("schedule.txt", "w") as fd:
    fd.write(str(s.schedule))

with open("scores.txt", "w") as fd:
    fd.write(str(all_happiness_scores))
